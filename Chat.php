<?php

$serveur ='localhost' ;
$username ='bdd_9u2p' ; 
$password ='Fromage41' ;
$bdd =  new PDO('mysql:host='.$serveur.';dbname='.$username,$username,$password); # On crée un "objet" PDO , cet objet sert à se connecter à la base de données 
$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);   # 
if(isset($_POST['pseudo']) AND isset($_POST['message']) AND !empty($_POST['pseudo']) AND !empty($_POST['message']))
{
	 $pseudo = htmlspecialchars($_POST['pseudo']);
	 $message = htmlspecialchars($_POST['message'])	;	#htmlspecialchars permet de sécuriser le chat de manière à ce que l'on ne puisse pas entrer de code
	 $insertmsg = $bdd->prepare('INSERT INTO Chat(pseudo,message) VALUES(?,?)');
	 $insertmsg->execute(array($pseudo, $message));
}
?>


<html>
<head>
	<title>ChaTrasia</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div id="main">
		<h1>ChaTrasia</h1>
		<div id="info">
			<form  method="post" action="" > <!-- On fait un formulaire -->
				<input type="text" name="pseudo" placeholder="Pseudo" value="<?php if(isset($pseudo)) {echo $pseudo; }?>"><br><br> <!-- le php dans value permet de garder le pseudo en mémoire -->
				<textarea type="text" name="message" placeholder="Message"></textarea><br><br> 
				<input type="submit" value="Envoyer"></input><br><br>
				<article>
				<?php
					$allmsg = $bdd->query('SELECT * FROM Chat ORDER BY id DESC LIMIT 0,15');
					while($msg = $allmsg->fetch())
					{
						?>
						<b><?php echo $msg['pseudo']; ?> :</b> <?php echo $msg['message']; ?> <br/>
						<?php
					}
					$allmsg->closeCursor();
				?>
				</article>
		</div>
	</div>
</body>